import hashlib


def main():

    hasheo = str(input(
        "Introduzca el tipo de hashing a resolver. \nPuede ser md5, sha1, sha224, sha256, sha384 o sha512: ")).lower()
    resolver_hash = ''

    if hasheo == 'md5':
        resolver_hash = str(input("Hash md5 a resolver: "))
    elif hasheo == 'sha1':
        resolver_hash = str(input("Hash sha1 a resolver: "))
    elif hasheo == 'sha224':
        resolver_hash = str(input("Hash sha224 a resolver: "))
    elif hasheo == 'sha256':
        resolver_hash = str(input("Hash sha256 a resolver: "))
    elif hasheo == 'sha384':
        resolver_hash = str(input("Hash sha384 a resolver: "))
    elif hasheo == 'sha512':
        resolver_hash = str(input("Hash sha512 a resolver: "))
    else:
        print('El metodo de hasheo introducido no es valido. Terminando programa.')
        return

    claves = open("millonclavesmascomunes.txt", "r")

    for clave in claves.readlines():

        linea = clave.strip("\n")
        linea = linea.encode('utf-8')
        
        if hasheo == 'md5':
            linea = hashlib.md5(linea).hexdigest()
        elif hasheo == 'sha1':
            linea = hashlib.sha1(linea).hexdigest()
        elif hasheo == 'sha224':
            linea = hashlib.sha224(linea).hexdigest()
        elif hasheo == 'sha256':
            linea = hashlib.sha256(linea).hexdigest()
        elif hasheo == 'sha384':
            linea = hashlib.sha384(linea).hexdigest()
        elif hasheo == 'sha512':
            linea = hashlib.sha512(linea).hexdigest()


        if linea == resolver_hash:
            print("Contrasena: {} \nHash Resuelto: {} ".format(clave, linea))
            break


if __name__ == '__main__':
    main()
